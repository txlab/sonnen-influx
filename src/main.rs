use anyhow::{anyhow, Context, Result};
use influxdb_client::{Client, Point, Precision, TimestampOptions};
use serde_json::{Map, Value};
use std::{thread::sleep, time::Duration};

mod app_config;
use crate::app_config::AppConfig;

// const FIELDS: [&str; 1] = ["Consumption_W"];

// #[derive(Debug, Deserialize)]
// #[allow(non_snake_case)]
// struct APIResult {
//     // BackupBuffer: String,
//     Consumption_W: i64,
// }

#[tokio::main]
async fn main() -> Result<()> {
    let cfg = AppConfig::new().context("loading config")?;

    if let Some(dsn) = &cfg.sentry_dsn {
        println!("Initializing Sentry for {:?}", sentry::release_name!());
        let _guard = sentry::init((
            dsn.clone(),
            sentry::ClientOptions {
                release: sentry::release_name!(),
                ..Default::default()
            },
        ));
    }

    let client = Client::new(&cfg.influx.url, &cfg.influx.token)
        .with_org(&cfg.influx.org)
        .with_bucket(&cfg.influx.bucket)
        .with_precision(Precision::S);

    loop {
        let result = do_it(&cfg, &client).await;
        if let Err(err) = result {
            eprintln!("Error: {:?}", err);
            sentry_anyhow::capture_anyhow(&err);
        };

        println!("Sleeping 10s\n");
        sleep(Duration::from_secs(10));
    }
}

async fn do_it(cfg: &AppConfig, client: &Client) -> Result<(), anyhow::Error> {
    println!("Querying: {}", cfg.api_url);
    let response = reqwest::get(&cfg.api_url).await;

    // on request error - continue loop after sleep
    if response.as_ref().is_err_and(|err| err.is_request()) {
        eprintln!("Request Error: {:?}", response.err());
    } else {
        let response = response?; // other errors should be thrown here

        let result: Map<String, Value> = response.json().await.context("parse JSON response")?;
        let mut points: Vec<Point> = vec![];

        for (field, value) in result.iter() {
            // dbg!(field, value);
            let mut point = Point::new(field)
                // .tag("batterie", "GME")
                .timestamp(chrono::offset::Utc::now().timestamp());

            match value {
                Value::Number(n) if n.is_f64() => {
                    point = point.field("value", n.as_f64().unwrap());
                }
                Value::Number(n) if n.is_i64() => {
                    point = point.field("value", n.as_i64().unwrap());
                }
                Value::Bool(b) => {
                    point = point.field("value", *b);
                }
                Value::String(s) => {
                    point = point.field("value", s.as_str());
                }
                _ => anyhow::bail!("Unhandled type of '{}': {:?}", field, value),
            }

            // dbg!(&point);
            points.push(point);
            // break;
        }

        println!("Inserting {} points", points.len());
        client
            .insert_points(&points, TimestampOptions::FromPoint)
            .await
            .map_err(|e| anyhow!("InfluxDB error: {:?}", e))
            .context("Failed to insert points into InfluxDB")?;
    }
    Ok(())
}
