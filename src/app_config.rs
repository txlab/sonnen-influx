use config::{Config, ConfigError};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct AppConfig {
    pub influx: Influx,
    pub api_url: String,
    pub sentry_dsn: Option<String>,
}
#[derive(Debug, Deserialize)]
pub struct Influx {
    pub url: String,
    pub org: String,
    pub bucket: String,
    pub token: String,
}

// pub fn init() -> Result<Config, ConfigError> {
impl AppConfig {
    pub fn new() -> Result<Self, ConfigError> {
        // println!("{:?}", config::File::with_name("config.yml"));
        let config = Config::builder()
            // Add in `./Settings.toml`
            .add_source(config::File::with_name("config.yml"))
            // Add in settings from the environment (with a prefix of APP)
            // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
            .add_source(config::Environment::with_prefix("APP"))
            .build()?;

        config.try_deserialize()
    }
}

// #[derive(/* Default, */ Debug /* , Serialize, Deserialize */)]
// pub struct MyConfig {
//     api_url: Option<String>,
// }
// `MyConfig` implements `Default`
// impl ::std::default::Default for MyConfig {
//     fn default() -> Self {
//         Self {
//             version: 0,
//             api_url: None,
//         }
//     }
// }
// let cfg: MyConfig = confy::load("sonnen-influx", None)?;
// println!(
//     "Config [{}] =o {:#?}",
//     confy::get_configuration_file_path("sonnen-influx", None)?.display(),
//     cfg
// );
