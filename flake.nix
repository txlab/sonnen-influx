{
  description = "Scrape SonnenBatterie API to InfluxDB";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    devenv.url = "github:cachix/devenv";
    utils.url = "github:numtide/flake-utils";
    nix2container = {
      url = "github:nlewo/nix2container";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane.url = "github:ipetkov/crane";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, systems, flake-parts, nixpkgs, fenix, crane, devenv, nix2container, rust-overlay, ... }: (
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = (import systems);
      imports = [
        inputs.devenv.flakeModule
      ];

      # perSystem docs: https://flake.parts/module-arguments.html#persystem-module-parameters
      perSystem = { config, self', inputs', pkgs, system, ... }: (
        let
          name = "sonnen-influx";
          # the system you want to build for:
          crossSystem = "aarch64-linux"; # raspi 4

          # rustToolchain = with fenix.packages.${system}; combine [
          #   minimal.rustc
          #   minimal.cargo
          #   targets.x86_64-unknown-linux-musl.latest.rust-std
          #   # targets.x86_64-unknown-linux-gnu.latest.rust-std
          #   # targets.wasm-unknown-unknown.latest.rust-std
          # ];

          # naerskLib = naersk.lib.${system}.override {
          #   cargo = rustToolchain;
          #   rustc = rustToolchain;
          # };
          # arbi = naerskLib.buildPackage {
          #   src = ./.;
          #   doCheck = false;

          #   # FROM: https://github.com/nix-community/naersk/blob/master/examples/static-musl/flake.nix
          #   buildInputs = with pkgs; [ pkg-config openssl openssl.dev ];
          #   nativeBuildInputs = with pkgs; [ pkg-config openssl openssl.dev pkgsStatic.stdenv.cc perl ];
          #   propagatedBuildInputs = with pkgs; [ pkg-config openssl openssl.dev ];
          #   CARGO_BUILD_TARGET = "x86_64-unknown-linux-musl";
          #   CARGO_BUILD_RUSTFLAGS = "-C target-feature=+crt-static";
          # };

          # FROM: https://crane.dev/examples/cross-rust-overlay.html
          pkgs = import nixpkgs {
            inherit crossSystem;
            localSystem = system;
            overlays = [ (import rust-overlay) ];
          };

          craneLib = (crane.mkLib pkgs).overrideToolchain (p: p.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
            extensions = [ "rust-src" ];
            targets = [ "aarch64-unknown-linux-musl" ];
          }));

          crateExpression =
            { openssl
            , libiconv
            , lib
            , pkg-config
            , qemu
            , stdenv
            }:
            craneLib.buildPackage {
              src = craneLib.cleanCargoSource ./.;
              strictDeps = true;

              # Build-time tools which are target agnostic. build = host = target = your-machine.
              # Emulators should essentially also go `nativeBuildInputs`. But with some packaging issue,
              # currently it would cause some rebuild.
              # We put them here just for a workaround.
              # See: https://github.com/NixOS/nixpkgs/pull/146583
              depsBuildBuild = [
                qemu
              ];

              # Dependencies which need to be build for the current platform
              # on which we are doing the cross compilation. In this case,
              # pkg-config needs to run on the build platform so that the build
              # script can find the location of openssl. Note that we don't
              # need to specify the rustToolchain here since it was already
              # overridden above.
              nativeBuildInputs = [
                pkg-config
                stdenv.cc
              ] ++ lib.optionals stdenv.buildPlatform.isDarwin [
                libiconv
              ];

              # Dependencies which need to be built for the platform on which
              # the binary will run. In this case, we need to compile openssl
              # so that it can be linked with our executable.
              buildInputs = [
                # Add additional build inputs here
                openssl
              ];

              # Tell cargo about the linker and an optional emulater. So they can be used in `cargo build`
              # and `cargo run`.
              # Environment variables are in format `CARGO_TARGET_<UPPERCASE_UNDERSCORE_RUST_TRIPLE>_LINKER`.
              # They are also be set in `.cargo/config.toml` instead.
              # See: https://doc.rust-lang.org/cargo/reference/config.html#target
              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER = "${stdenv.cc.targetPrefix}cc";
              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_RUNNER = "qemu-aarch64";

              # Tell cargo which target we want to build (so it doesn't default to the build system).
              # We can either set a cargo flag explicitly with a flag or with an environment variable.
              # cargoExtraArgs = "--target aarch64-unknown-linux-gnu";
              CARGO_BUILD_TARGET = "aarch64-unknown-linux-musl";
              CARGO_BUILD_RUSTFLAGS = "-C target-feature=+crt-static";

              # These environment variables may be necessary if any of your dependencies use a
              # build-script which invokes the `cc` crate to build some other code. The `cc` crate
              # should automatically pick up on our target-specific linker above, but this may be
              # necessary if the build script needs to compile and run some extra code on the build
              # system.
              HOST_CC = "${stdenv.cc.nativePrefix}cc";
              TARGET_CC = "${stdenv.cc.targetPrefix}cc";
            };

          # Assuming the above expression was in a file called myCrate.nix
          # this would be defined as:
          # my-crate = pkgs.callPackage ./myCrate.nix { };
          package = pkgs.callPackage crateExpression { };

          # craneLib = (crane.mkLib pkgs).overrideToolchain rustToolchain;
          # package = craneLib.buildPackage {
          #   # https://crane.dev/getting-started.html
          #   src = craneLib.cleanCargoSource (craneLib.path ./.);
          #   meta.mainProgram = name;

          #   # CARGO_BUILD_TARGET = "wasm-unknown-unknown";
          #   CARGO_BUILD_TARGET =
          #     let
          #       arch = builtins.elemAt (builtins.split "-" system) 0;
          #     in
          #     "${arch}-unknown-linux-musl";
          #   CARGO_BUILD_RUSTFLAGS = "-C target-feature=+crt-static";
          #   # CARGO_BUILD_TARGET = "x86_64-unknown-linux-gnu";

          #   doCheck = false;
          #   # # nativeBuildInputs = with pkgs; [ perl ];
          #   # buildInputs = with pkgs; [ pkg-config openssl openssl.dev ];
          #   # nativeBuildInputs = with pkgs; [ pkg-config openssl openssl.dev pkgsStatic.stdenv.cc perl ];
          #   # propagatedBuildInputs = with pkgs; [ pkg-config openssl openssl.dev ];
          #   # buildInputs = with pkgs; [ pkg-config openssl openssl.dev ];
          #   # nativeBuildInputs = with pkgs; [ pkg-config openssl openssl.dev ];
          #   # propagatedBuildInputs = with pkgs; [ pkg-config openssl openssl.dev ];
          # };
        in
        rec {

          # PACKAGES
          packages = {
            ${name} = package;
            default = packages.${name};
            docker = pkgs.dockerTools.buildImage {
              inherit name;
              config = {
                Cmd = [ (pkgs.getExe packages.default) ];
              };
              # copyToRoot = pkgs.buildEnv {
              #   name = "image-root";
              #   paths = with pkgs; [  ];
              #   pathsToLink = [ "/bin" ];
              # };
            };
          };

          # DEVSHELL
          devenv.shells.default = {
            # Useful packages for nix, so I put them here instead of devenv.nix
            packages = with pkgs;
              [
                nixpkgs-fmt
                nil
              ];
          } // (import ./devenv.nix { inherit pkgs; });

        }
      );
    });
}
