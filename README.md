# Scraper for SonnenBatterie API

## Development Setup

This repo uses [Nix Flakes](https://nixos.wiki/wiki/Flakes) from the get-go, but compat is provided for traditional nix-shell/nix-build as well (see the section below).

```bash
# Dev shell
nix develop

# or just run directly
nix run

# or run via cargo
nix develop -c cargo run

# build
nix build
```

(for legacy nix & other details [see the template docs](https://github.com/srid/rust-nix-template))